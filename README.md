Container image for fetachmail
==============================

This is a container image definition based on the Fedora Linux
operating system. It packages the [fetchmail](https://www.fetchmail.info/)
email fetching utility.

Configuration & Running
-----------------------

The container is designed to just start `fetchmail --nodetach`. All
configurations should be specified in a config file, and that config
file should be exported to fetchmail using the `FETCHMAILHOME` environment
variable, as specified by the
[fetchmail manual](https://www.fetchmail.info/fetchmail-man.html).

General gotchas
---------------

There are a number of gotchas when running Fetchmail from a container,
as opposed to localhost:

  - Delivery should not happen to the `localhost` MTA anymore; you
    need to specify the `smtphost` option in `fetchmailrc`. Note
	that your MTA might request a proper domain name, not an IP
	address.
	
  - The MTA needs to accept emails from your container IP. With
    Postfix, this might need that you need to set `mynetworks`
	accordingly.
	
  - Your MTA is probably configured to request a fully-qualified domain
    name (FQDN) as recipient address. This means that your container
	needs a proper hostname.

Examples
--------

Here's an example `fetchmailrc` file:
```
    # General options
    set postmaster "postmaster"
    set daemon 60

    # Needs a domain name here
    defaults smtphost mail.rootshell.ro

    poll pop.gmx.net with proto POP3
           user 'xxxxx' there with
           password 's3cr3t' is 'me' here
           ssl
```

Run the fetchmail image with the following command:
```
    $ podman run -ti -e FETCHMAILHOME=/conf -v /conf/on/host:/conf:z \
	      --name pod-fetchmail --hostname=fetchmail.my-host.net \
		  registry.gitlab.com/codedump2/fetchmail-image:latest
```

Making the pod persistent
-------------------------

You can create a systemd service:
```
    $ podman generate systemd pod-fetchmail > /etc/systemd/system/pod-fetchmail.service
	$ systemctl start pod-fetchmail
	$ systemctl enable pod-fetchmail
```


