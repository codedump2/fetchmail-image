FROM registry.fedoraproject.org/fedora:35

RUN \
	dnf -y update && \
	dnf -y install fetchmail

ENTRYPOINT fetchmail --nodetach

